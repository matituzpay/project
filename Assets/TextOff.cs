﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextOff : MonoBehaviour
{
    public void ErrorTextOff()
    {
        this.gameObject.SetActive(false);
    }
}
