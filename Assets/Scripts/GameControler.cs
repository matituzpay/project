﻿using DigitalRuby.PyroParticles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControler : MonoBehaviour
{
    float MissionTime , TimeStartMisson = 3f, TimeToQuench = 5;
    public GameObject StartAlarmObject, FireObject, FireExtinguisher, FireExtinguisherInHand, Helmet, Jacket, EndGameUi;
    public Text TimeText, EndGameText, ErrorText;
    public ParticleSystem Particle, Particle2;
    bool Extinction = false;
    public void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        EndGameUi.SetActive(false);
        Cursor.visible = false;
        Time.timeScale = 1;
    }
    void Update()
    {
        Ray rayfire = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitfire;
        if (Physics.Raycast(rayfire, out hitfire))
        {
            if (hitfire.collider.name == FireObject.name)
            {
                if (FireExtinguisher.active == false)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (Input.GetMouseButtonUp(0))
                        {
                            Extinction = false;
                        }
                        else
                        {
                            Extinction = true;
                        }
                    }
                }
            }
        }
        TimeStartMisson -= Time.deltaTime;
        if(TimeStartMisson < 0)
        {
            MissionTime += Time.deltaTime;
            TimeText.text = MissionTime.ToString("F2") + "s";
            StartAlarmObject.SetActive(true);
        }
        if (Extinction == true)
        {
            TimeToQuench -= Time.deltaTime;
            Particle.gameObject.GetComponent<ParticleSystemRenderer>().minParticleSize = TimeToQuench / 5;
        }
        if (TimeToQuench < 0f)
        {
            Cursor.lockState = CursorLockMode.None;
            EndGameText.text = "Wygrałeś! Twój czas to: " + MissionTime.ToString("F2") + "s";
            Time.timeScale = 0;
            if(PlayerPrefs.GetFloat("MissionTime") > MissionTime)
            {
                PlayerPrefs.SetFloat("MissionTime", MissionTime);
            }
            StartAlarmObject.SetActive(false);
            EndGame();
        }
        ButtonDown();
        ButtonUp();
    }
    public void ButtonDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (FireExtinguisherInHand.active == true)
            {
                Particle2.startLifetime = 0.2f;
            }
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.name == FireExtinguisher.name)
                {
                    if (Helmet.active == false && Jacket.activeSelf == false)
                    {
                        FireExtinguisher.SetActive(false);
                        FireExtinguisherInHand.SetActive(true);
                        ErrorText.gameObject.SetActive(false);
                    }
                    else
                    {
                        ErrorText.gameObject.SetActive(true);
                        ErrorText.text = "Nie ubrałeś hełmu i kamizelki!";
                    }
                }
                if (hit.collider.name == Helmet.name)
                {
                    Helmet.SetActive(false);
                }
                if (hit.collider.name == Jacket.name)
                {
                    if (Helmet.active == false)
                    {
                        Jacket.SetActive(false);
                        ErrorText.gameObject.SetActive(false);
                    }
                    else
                    {
                        ErrorText.gameObject.SetActive(true);
                        ErrorText.text = "Nie ubrałeś hełmu!";
                    }
                }
            }
        }
    }
    public void ButtonUp()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (TimeToQuench > 0)
            {
                Extinction = false;
            }
            if (FireExtinguisherInHand.active == true)
            {
                Particle2.startLifetime = 0;
            }
        }
    }
    void EndGame()
    {
        Cursor.visible = true;
        EndGameUi.SetActive(true);
    }
    public void RestartButoon()
    {
        SceneManager.LoadScene(1);
    }
}
