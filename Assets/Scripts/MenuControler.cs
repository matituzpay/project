﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuControler : MonoBehaviour
{
    public Text StatsText, NameText, PlaceHolderText;
    public GameObject InputField;
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        LoadStats();
    }
    public void LoadGame()
    {
        if(PlayerPrefs.GetString("PlayerName") != "")
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            PlaceHolderText.text = "wpisz swój nick!";
        }
    }
    void LoadStats()
    {
        if (PlayerPrefs.GetFloat("MissionTime") != 0)
        {
            InputField.SetActive(false);
            StatsText.gameObject.SetActive(true);
            NameText.gameObject.SetActive(false);
            StatsText.text = PlayerPrefs.GetString("PlayerName") + " twój rekod to " + PlayerPrefs.GetFloat("MissionTime").ToString("F2");
        }
    }
    public void SaveName()
    {
        PlayerPrefs.SetString("PlayerName", NameText.text);
    }
}
